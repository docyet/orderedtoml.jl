using Test
using Dates

using OrderedTOML: OrderedTOML, parse, tryparse, ParserError, Internals, print

function roundtrip(data)
    mktemp() do file, io
        data_parsed = OrderedTOML.parse(data)
        OrderedTOML.print(io, data_parsed)
        close(io)
        data_roundtrip = OrderedTOML.parsefile(file)
        return isequal(data_parsed, data_roundtrip)
    end
end

include("readme.jl")
include("toml_test.jl")
include("values.jl")
include("invalids.jl")
include("error_printing.jl")
include("print.jl")
include("parse.jl")

@inferred OrderedTOML.parse("foo = 3")


str = """
# This is a TOML document.

title = "TOML Example"

[owner]
name = "Tom Preston-Werner"
"""
roundtrip(str)

d = OrderedTOML.tryparse(str)
# @test d["title"] == "TOML Example"
# @test d["owner"]["name"] == "Tom Preston-Werner"
# @test d["database"] == Dict(
#   "server" => "192.168.1.1",
#   "ports" => [ 8001, 8001, 8002 ],
#   "connection_max" => 5000,
#   "enabled" => true,
# )
# @test d["servers"] == Dict(
#   "alpha" => Dict(
#   "ip" => "10.0.0.1",
#   "dc" => "eqdc10",
#   ),
#   "beta" => Dict(
#     "ip" => "10.0.0.2",
#     "dc" => "eqdc10",
#   )
# )
