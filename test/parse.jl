using OrderedTOML, Test
using OrderedTOML: ParserError

@testset "OrderedTOML.(try)parse(file) entrypoints" begin
    dict = Dict{String,Any}("a" => 1)
    str = "a = 1"; invalid_str = "a"
    path, io = mktemp(); write(io, str); close(io)
    invalid_path, io = mktemp(); write(io, invalid_str); close(io)
    p = OrderedTOML.Parser()
    # OrderedTOML.parse
    @test OrderedTOML.parse(str) == OrderedTOML.parse(SubString(str)) ==
          OrderedTOML.parse(IOBuffer(str)) ==
          OrderedTOML.parse(p, str) == OrderedTOML.parse(p, SubString(str)) ==
          OrderedTOML.parse(p, IOBuffer(str)) == dict
    @test_throws ParserError OrderedTOML.parse(invalid_str)
    @test_throws ParserError OrderedTOML.parse(SubString(invalid_str))
    @test_throws ParserError OrderedTOML.parse(IOBuffer(invalid_str))
    @test_throws ParserError OrderedTOML.parse(p, invalid_str)
    @test_throws ParserError OrderedTOML.parse(p, SubString(invalid_str))
    @test_throws ParserError OrderedTOML.parse(p, IOBuffer(invalid_str))
    # OrderedTOML.tryparse
    @test OrderedTOML.tryparse(str) == OrderedTOML.tryparse(SubString(str)) ==
          OrderedTOML.tryparse(IOBuffer(str)) ==
          OrderedTOML.tryparse(p, str) == OrderedTOML.tryparse(p, SubString(str)) ==
          OrderedTOML.tryparse(p, IOBuffer(str)) == dict
    @test OrderedTOML.tryparse(invalid_str) isa ParserError
    @test OrderedTOML.tryparse(SubString(invalid_str)) isa ParserError
    @test OrderedTOML.tryparse(IOBuffer(invalid_str)) isa ParserError
    @test OrderedTOML.tryparse(p, invalid_str) isa ParserError
    @test OrderedTOML.tryparse(p, SubString(invalid_str)) isa ParserError
    @test OrderedTOML.tryparse(p, IOBuffer(invalid_str)) isa ParserError
    # OrderedTOML.parsefile
    @test OrderedTOML.parsefile(path) == OrderedTOML.parsefile(SubString(path)) ==
          OrderedTOML.parsefile(p, path) == OrderedTOML.parsefile(p, SubString(path)) == dict
    @test_throws ParserError OrderedTOML.parsefile(invalid_path)
    @test_throws ParserError OrderedTOML.parsefile(SubString(invalid_path))
    @test_throws ParserError OrderedTOML.parsefile(p, invalid_path)
    @test_throws ParserError OrderedTOML.parsefile(p, SubString(invalid_path))
    # OrderedTOML.tryparsefile
    @test OrderedTOML.tryparsefile(path) == OrderedTOML.tryparsefile(SubString(path)) ==
          OrderedTOML.tryparsefile(p, path) == OrderedTOML.tryparsefile(p, SubString(path)) == dict
    @test OrderedTOML.tryparsefile(invalid_path) isa ParserError
    @test OrderedTOML.tryparsefile(SubString(invalid_path)) isa ParserError
    @test OrderedTOML.tryparsefile(p, invalid_path) isa ParserError
    @test OrderedTOML.tryparsefile(p, SubString(invalid_path)) isa ParserError
end
